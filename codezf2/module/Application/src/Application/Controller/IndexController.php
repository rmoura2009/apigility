<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        
        $adapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $result = $adapter->query('Select * from users');
        
        foreach ($result->execute() as $r){
            echo $r['username'].'<br>';
        }
        $nome = $this->getServiceLocator()->get('AppName');
        return new ViewModel(['nome' => $nome]);
    }
}
